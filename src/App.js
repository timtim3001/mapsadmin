import React from 'react';
import { Admin, Resource } from 'react-admin';
import authProvider from './authProvider';
import simpleRestProvider from './dataProvider';
import KeysList from './KeysList';
import {KeyCreate, KeyEdit} from './keys';

const dataProvider = simpleRestProvider('http://145.48.6.80:3000');

const App = () => (
    <Admin  dataProvider={dataProvider} authProvider={authProvider}>
        <Resource name="keys" list={KeysList} create={KeyCreate} edit={KeyEdit}/>
    </Admin>
);

export default App;
