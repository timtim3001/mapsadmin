import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR } from 'react-admin';

export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        let buf = Buffer.from(`${username}:${password}`);
        let encoded = buf.toString('base64');
        const request = new Request('http://145.48.6.80:3000/keys', {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': `Basic ${encoded}`}),
        })
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token }) => {
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
            });
    }

    if(type === AUTH_LOGOUT) {
        localStorage.removeItem('username');
        localStorage.removeItem('password');
        return Promise.resolve();
    }

    if (type === AUTH_ERROR) {
        const status  = params.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('username');
            localStorage.removeItem('password');
            return Promise.reject();
        }
        return Promise.resolve();
    }
    return Promise.resolve();
}