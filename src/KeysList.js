import React from 'react';
import { List, Datagrid, TextField, EditButton} from 'react-admin';


export default class KeysList extends React.Component{

    render() {
        return(
            <List {...this.props}>
                <Datagrid>
                    <TextField source="name" />
                    <TextField source="key" />
                    <TextField source="quota" />
                    <EditButton/>
                </Datagrid>
            </List>
        );
    }

}