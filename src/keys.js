import React from 'react';
import { Create, Edit, SimpleForm, DisabledInput, TextInput } from 'react-admin';
import { required } from 'ra-core';

export const KeyCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="key" />
        </SimpleForm>
    </Create>
)

export const KeyEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <DisabledInput source="name" />
            <TextInput source="quota" validate={required()}/>
        </SimpleForm>
    </Edit>
)